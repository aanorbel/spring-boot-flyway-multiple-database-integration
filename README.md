# Spring Boot Multiple Datasource with Liquibase
## Multiple Datasource
You need to add a couple of datasource definitions to your yaml configuration file.

    datasource:
      primary:
        url: jdbc:mysql://localhost/primary
        username: root
        driver-class-name: com.mysql.jdbc.Driver
        validation-query: select 1
      secondary:
        url: jdbc:mysql://localhost/secondary
        username: root
        driver-class-name: com.mysql.jdbc.Driver
        validation-query: select 1

Make sure to disable the *DataSourceAutoConfiguration* in you configuration

    @EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})

You then need to manual configure each datasource and remember to set one as primary for any other auto configuration to work.

    @Bean(name = "primaryDataSource")
    @Primary
    @ConfigurationProperties(prefix="datasource.primary")
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "jdbcPrimaryTemplate")
    public JdbcTemplate jdbcPrimaryTemplate(@Qualifier(value = "primaryDataSource") DataSource primaryDataSource) {
        return new JdbcTemplate(primaryDataSource);
    }


## Flyway
### Running Flyway
All you need to do in a Spring Boot project to get Flyway to run for you is to simply add a dependency to the project pom. You can read more in the documents.
[Spring boot  Database Initialization](http://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html)
[Flyway  Database Initialization](https://flywaydb.org/documentation/api/#programmatic-configuration-java)